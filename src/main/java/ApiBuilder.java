import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.LinkedHashMap;
import java.util.Map;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiBuilder {
    public String kind;
    public String etag;
    public String discoveryVersion;
    public String id;
    public String name;
    public String version;
    public String revision;
    public String title;
    public String description;
    public String ownerDomain;
    public String ownerName;
    public String documentationLink;
    public String protocol;
    public String baseUrl;
    public String basePath;
    public String rootUrl;
    public String servicePath;
    public String batchPath;

    // Schemas
    public LinkedHashMap<String, Object> schemas;

    // Resources
    public LinkedHashMap<String, Object> resources;

}
