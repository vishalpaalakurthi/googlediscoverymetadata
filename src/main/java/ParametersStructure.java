import java.util.LinkedHashMap;
import java.util.Map;

public class ParametersStructure {
    Map<String, Object> properties = new LinkedHashMap<>();

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public ParametersStructure() {

    }


    public LinkedHashMap<String ,Object> createParams() {
       return new LinkedHashMap<>();
    }
}
