import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.wnameless.json.flattener.JsonFlattener;
import com.github.wnameless.json.unflattener.JsonUnflattener;
import groovy.lang.Tuple2;
import io.dxchange.rest.designcenter.common.JsonSchemaGenerator;
import io.dxchange.rest.designcenter.common.JsonSchemaToJsonStructure;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;

public class GoogleDiscoveryExtractor {

    static ObjectMapper objectMapper = new ObjectMapper();
    static ApiBuilder apiBuilder;
    static ParametersStructure parametersStructure = new ParametersStructure();

    public static void main(String[] args) throws IOException {

        // Read File
//        File file = new File("src/main/resources/googlesheets.json");
//        File file = new File("src/main/resources/googledrive.json");
//        String data = Files.readString(file.toPath());
        URL url = new URL("https://www.googleapis.com/discovery/v1/apis/drive/v3/rest");
//        URL url = new URL("https://sheets.googleapis.com/$discovery/rest?version=v4");
        JSONObject json = new JSONObject(IOUtils.toString(url, Charset.forName("UTF-8")));
        apiBuilder = objectMapper.readValue(json.toString(), ApiBuilder.class);
        buildSchemas();
        buildResources();
        parseResourcesWithName(apiBuilder.getResources());
        putInExcelSheet(apiBuilder.getName().split("\\.")[0]);

    }

    private static void buildSchemas() {
        LinkedHashMap<String, Object> schemas = new LinkedHashMap<>(apiBuilder.getSchemas());
        schemas.forEach((s, o) -> {
            Map<String, Object> ppp = parseSchemas(o);
            Map<String, Object> map = JsonUnflattener.unflattenAsMap(ppp);
            apiBuilder.getSchemas().put(s, map);
        });
    }

    @SneakyThrows
    public static Map<String, Object> parseSchemas(Object value) {
        Map<String, Object> map = JsonFlattener.flattenAsMap(objectMapper.writeValueAsString(value));
        LinkedHashMap<String, Object> clonedMap = new LinkedHashMap<>(map);
        for (Map.Entry<String, Object> entry : clonedMap.entrySet()) {
            String s = entry.getKey();
            Object o = entry.getValue();
            if (s.contains("$ref")) {
                String refValue = String.valueOf(o);
                String s1 = s.replace(".$ref", "." + refValue.toLowerCase());
                map.put(s1, parseSchemas(apiBuilder.getSchemas().get(refValue)));
                map.remove(s);
            }
        }
        return JsonUnflattener.unflattenAsMap(map);
    }

    private static void buildResources() {
        LinkedHashMap<String, Object> schemas = new LinkedHashMap<>(apiBuilder.getResources());
        schemas.forEach((s, o) -> {
            Map<String, Object> ppp = parseResources(o);
            Map<String, Object> map = JsonUnflattener.unflattenAsMap(ppp);
            apiBuilder.getResources().put(s, map);
        });
    }

    @SneakyThrows
    public static Map<String, Object> parseResources(Object value) {
        Map<String, Object> map = JsonFlattener.flattenAsMap(objectMapper.writeValueAsString(value));
        LinkedHashMap<String, Object> clonedMap = new LinkedHashMap<>(map);
        for (Map.Entry<String, Object> entry : clonedMap.entrySet()) {
            String s = entry.getKey();
            Object o = entry.getValue();
            if (s.contains("$ref")) {
                String refValue = String.valueOf(o);
                String s1 = s.replace(".$ref", "");
                map.put(s1, apiBuilder.getSchemas().get(refValue));
                map.remove(s);
            }
        }
        return JsonUnflattener.unflattenAsMap(map);
    }

    private static Map parseResourcesWithName(Map map) {
        boolean isExistResources = map.containsKey("resources");
        Map<String, Object> resources = new LinkedHashMap<>();
        if (isExistResources) {
            resources = (Map<String, Object>) map.get("resources");
        } else {
            resources = map;
        }
        Set<String> keys = new HashSet<>(resources.keySet());
        for (String key : keys) {
            Map<String, Object> data = (Map<String, Object>) resources.get(key);
            if (data.containsKey("resources")) {
                Map map1 = new HashMap();
                map1.put("resources", data.get("resources"));
                parseResourcesWithName(map1);
            }
            if (data.containsKey("methods")) {
                Map<String, Object> map1 = addMethodsToMetadata(key, (Map) data.get("methods"));
                apiBuilder.getResources().remove(key);
                apiBuilder.getResources().putAll(map1);
            }
        }
        return map;

    }

    private static Map<String, Object> addMethodsToMetadata(String key, Map methods) {
        Map<String, Object> mmm = new LinkedHashMap<>();
        Set set = methods.keySet();
        set.forEach(o -> {
            try {
                Map<String, Object> o1 = (Map<String, Object>) methods.get(o);
                String id = (String) o1.get("id");
                Tuple2<String, String> keyNamesForRoot = getKeyNamesForRoot(id);
                Map<String, Object> params = getParameters(o1.get("parameterOrder"), o1.get("parameters"));

                if (o1.get("request") != null) {
                    params.put("body", o1.get("request"));
                }

                Map<String , Object> rootParams = new LinkedHashMap<>();
                rootParams.put(keyNamesForRoot.getSecond() + "Input", params);

                JsonSchemaGenerator.Schema schema = null;
                schema = JsonSchemaGenerator.jsonToSchema(objectMapper.writeValueAsString(rootParams), "id", false);
                String template = JsonSchemaToJsonStructure.toJsonStructure(schema, null, "Template");
                o1.put("parameters", template);

//                Object response = o1.get("response");
//                if (response != null) {
//                    String responseTemplate = null;
//                    ParametersStructure parametersStructure = new ParametersStructure();
//                    LinkedHashMap<String, Object> params1 = parametersStructure.createParams();
//                    params1.putAll((Map<? extends String, ?>) response);
//                    try {
//                        responseTemplate = JsonSchemaToJsonStructure.toJsonStructure(objectMapper.writeValueAsString(params1), null, "Template");
//                    } catch (Exception ex) {
//                        JsonSchemaGenerator.Schema responseSchema = JsonSchemaGenerator.jsonToSchema(objectMapper.writeValueAsString(params1), "id", false);
//                        responseTemplate = JsonSchemaToJsonStructure.toJsonStructure(objectMapper.writeValueAsString(responseSchema), null, "Template");
//
//                    }
//                    o1.put("response", responseTemplate);
//                }
                mmm.put(keyNamesForRoot.getFirst(), o1);
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
        return mmm;
    }

    private static Map<String, Object> getParameters(Object parameterOrder, Object parameters) {
        LinkedHashMap<String, Object> pathParams = parametersStructure.createParams();
        LinkedHashMap<String, Object> queryParams = parametersStructure.createParams();

        ArrayList<String> pmOrder = (ArrayList<String>) parameterOrder;
        LinkedHashMap<String, Object> pms = (LinkedHashMap<String, Object>) parameters;
        if (pmOrder == null) {
            if (pms != null) {
                pmOrder = new ArrayList<>(pms.keySet());
            } else {
                pmOrder = new ArrayList<>();
            }
        }
        pmOrder.forEach(s -> {
            Map<String, Object> paramMetadata = (Map<String, Object>) pms.get(s);
            String location = (String) paramMetadata.get("location");

            if (location.equalsIgnoreCase("path")) {
                Map<String, Object> o = (Map<String, Object>) pms.get(s);
                pathParams.put(s, getValueByDatatype((String) o.get("type")));
            } else if (location.equalsIgnoreCase("query")) {
                Map<String, Object> o = (Map<String, Object>) pms.get(s);
                queryParams.put(s, getValueByDatatype((String) o.get("type")));
            }
        });
        Map<String, Object> props = new LinkedHashMap<>();
        if (pathParams != null && pathParams.size() > 0)
            props.put("pathParams", pathParams);
        if (queryParams != null && queryParams.size() > 0)
            props.put("queryParams", queryParams);
        return props;
    }

    public static Object getValueByDatatype(String type) {
        if (type.equalsIgnoreCase("boolean")) {
            return Boolean.FALSE;
        } else if (type.equalsIgnoreCase("string")) {
            return "sample";
        } else if (type.equalsIgnoreCase("integer")) {
            return 0l;
        }
        return new Object();
    }


    @SneakyThrows
    private static void putInExcelSheet(String name) {
        File dir = new File(name);
        if(!dir.exists()){
            dir.mkdir();
        }
        try {
            for (Map.Entry<String, Object> entry : apiBuilder.getResources().entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                LinkedHashMap<String, Object> resource = (LinkedHashMap<String, Object>) value;

                Map map = new HashMap();
                map.put("Name", key);
                map.put("Description", resource.get("description"));
                map.put("Base Url", apiBuilder.getBaseUrl());
                map.put("Http Method", resource.get("httpMethod"));
                map.put("Path", resource.get("path"));
                map.put("Parameters", resource.get("parameters"));
                map.put("Response", resource.get("response"));
                map.put("Scopes", resource.get("scopes"));

                File specificFile = new File(name + File.separator + key.replaceAll(" ","_") + ".json");
                Files.write(specificFile.toPath(), objectMapper.writeValueAsString(map).getBytes());
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();

        }

        System.out.println("End.");
    }

    public static Tuple2<String, String> getKeyNamesForRoot(final String init) {
        if (init == null)
            return null;

        final StringBuilder ret = new StringBuilder(init.length());

        for (final String word : init.split("\\.")) {
            if (!word.isEmpty()) {
                ret.append(Character.toUpperCase(word.charAt(0)));
                ret.append(word.substring(1).toLowerCase());
            }
            if (!(ret.length() == init.length()))
                ret.append(" ");
        }

        String s = ret.toString();
        String s1 = s.replaceAll(" ", "");
        return new Tuple2<>(s,s1);
    }
}
