# **Discovery URLs**

**Google Docs & Google Discovery API's URL**
https://developers.google.com/discovery/v1/reference/apis/list

**Google Drive**
https://www.googleapis.com/discovery/v1/apis/drive/v3/rest

**Google Sheet**
https://sheets.googleapis.com/$discovery/rest?version=v4